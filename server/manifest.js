const Dotenv = require('dotenv');
const Confidence = require('confidence');
const Toys = require('toys');
const Boom = require('@hapi/boom');

const IP = require('ip');
const Package = require('../package');

// Pull .env into process.env
Dotenv.config({ path: `${__dirname}/.env` });

// Glue manifest as a confidence store
module.exports = new Confidence.Store({
  server: {
    host: { $env: 'HOST', $default: IP.address() },
    address: { $env: 'ADDRESS', $default: IP.address() },
    port: { $env: 'PORT', $coerce: 'number', $default: 3000 },
    debug: {
      $filter: { $env: 'NODE_ENV' },
      $default: {
        log: ['error', 'implementation', 'internal'],
        request: ['error', 'implementation', 'internal']
      },
      test: {
        log: false,
        request: false
      },
      production: {
        request: ['implementation']
      }
    },
    app: {
      name: { $env: 'APP_NAME', $default: Package.name },
      version: Package.version
    },
    routes: {
      $filter: { $env: 'NODE_ENV' },
      $base: {
        cors: {
          origin: ['*']
        }
      },
      development: {
        validate: {
          failAction: (request, h, err) => {
            request.log(['validationError'], err);

            return Boom.badRequest();
          }
        }
      }
    }
  },
  register: {
    plugins: [
      {
        plugin: 'schmervice'
      },
      {
        plugin: 'schwifty',
        options: {
          $filter: 'NODE_ENV',
          $default: {},
          $base: {
            migrateOnStart: true,
            knex: {
              client: 'postgres',
              connection: {
                host: { $env: 'POSTGRES_HOST', $default: 'postgres' },
                port: {
                  $env: 'POSTGRES_PORT',
                  $coerce: 'number',
                  $default: 5432
                },
                user: { $env: 'POSTGRES_USER', $default: 'hapi' },
                password: { $env: 'POSTGRES_PASSWORD', $default: 'hapi' },
                database: { $env: 'POSTGRES_DB', $default: Package.name }
              }
            }
          },
          production: {
            migrateOnStart: false
          }
        }
      },
      {
        plugin: './plugins/good'
      },
      {
        plugin: 'hapi-auth-jwt2'
      },
      {
        plugin: '../lib',
        options: {
          jwtKey: {
            $filter: { $env: 'NODE_ENV' },
            $default: {
              $env: 'JWT_SECRET',
              $default: 'jwt-secret'
            },
            production: {
              $env: 'JWT_SECRET'
            }
          }
        }
      },
      {
        plugin: {
          $filter: { $env: 'NODE_ENV' },
          $default: Toys.noop,
          development: './plugins/swagger'
        }
      },
      {
        plugin: 'blipp',
        options: {
          $filter: { $env: 'NODE_ENV' },
          $default: {},
          $base: { showAuth: true, showStart: true },
          production: { showAuth: false, showStart: false },
          test: { showAuth: false, showStart: false }
        }
      },
      {
        plugin: {
          $filter: { $env: 'NODE_ENV' },
          $default: 'hpal-debug',
          production: Toys.noop
        }
      }
    ]
  }
});
