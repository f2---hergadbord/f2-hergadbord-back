const Good = require('@hapi/good');
const Dotenv = require('dotenv');

// Pull .env into process.env
Dotenv.config({ path: `${__dirname}/.env` });

module.exports = {
  name: 'app-good',
  async register(server) {
    await server.register([
      {
        plugin: Good,
        options: {
          reporters: {
            myConsoleReporter: [
              {
                module: '@hapi/good-squeeze',
                name: 'Squeeze',
                args: [
                  {
                    log: process.env.NODE_ENV === 'test' ? 'none' : '*',
                    response: process.env.NODE_ENV === 'test' ? 'none' : '*',
                    error: '*',
                    request: '*'
                  }
                ]
              },
              {
                module: '@hapi/good-console',
                args: [{ format: 'DD/MM/YYYY HH:mm:ss', color: false }]
              },
              'stdout'
            ]
          }
        }
      }
    ]);
  }
};
