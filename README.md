# Slock backend

 Slock is the new slack! This is the backend of Slock.
 It's written in node.js using Hapi !
 
## Usage in local

### Requirements

#### Installation

You will need to have the following installed :
- Node.js
- PostgreSQL

To avoid the installation of PostgresSQL, you can use the repository DevEnv (located in the same GitLab group) to setup PostgreSQL using Docker.
In this case, you will need to have Docker and Docker-compose installed.

#### Setup

You will need to create a .env file inside server folder with the following env vars :
- NODE_ENV (development)
- PORT (ex : 4000)
- HOST (ex : localhost)
- ADDRESS (ex : 0.0.0.0)
- POSTGRES_HOST (ex : 0.0.0.0)
- POSTGRES_USER (ex : slock)
- POSTGRES_PASSWORD (ex : password)
- POSTGRES_DB (ex : slock_db)
- JWT_SECRET (Jwt private key)

All POSTGRES_ vars will need to reflect your configuration of PostgreSQL to be able to connect to is.
If you use DevEnv repository, you have to declare a .env at root folder containing only the POSTGRES_ vars.
To launch the database, use start.sh script and to stop it, use stop.sh script.
This will setup PostgreSQL for you and you will have to use the same vars inside the backend .env.

### Launch

To run it :
- npm i
- npm start

And enjoy !


## How to deploy

Each push on the master branch will trigger Gitlab CI/CD which will execute 2 stages :
- Test => run lint and unit tests, if one of those fail it will not go further
- Deploy => Deploy the app on Heroku !

As simple as that ;)

## App on heroku

The backend will be accessed at : https://f2-hergadbord-back.herokuapp.com/

We use free version of Heroku and Postgres add-on (limited connection). So if you experience an issue
regarding this, let us know!