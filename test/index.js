// Load modules

const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Hoek = require('@hapi/hoek');
const Teamwork = require('@hapi/teamwork');
const Io = require('socket.io-client');
const Faker = require('faker');

const Server = require('../server');
const Package = require('../package.json');

// Test shortcuts

exports.lab = Lab.script();

const { describe, it, before, beforeEach, afterEach, after } = exports.lab;
const { expect } = Code;

const Helpers = {
  authJWT(userId = 0) {
    return {
      auth: {
        strategy: 'jwt',
        credentials: {
          id: userId
        }
      }
    };
  },
  generateRoom(name, adminIds, userIds) {
    return { name, adminIds, userIds };
  },
  generateUser(
    email,
    pseudo,
    password = Faker.random.word(),
    idAvatar = Faker.random.number()
  ) {
    return { email, pseudo, password, idAvatar };
  }
};

let server;

describe('TestEnv', () => {
  before(async () => {
    server = await Server.deployment(true);
  });

  after(async () => {
    await server.stop();
  });

  describe('Deployment', () => {
    it('registers the main plugin.', async () => {
      expect(server.registrations[Package.name]).to.exist();
      expect(server.chat).to.exist();
    });
  });

  describe('Auth', () => {
    it('should fail to access route with bad credential in jwt (unauthorized)', async () => {
      const { userService } = server.services();
      const token = await userService.createToken(1);

      const { statusCode } = await server.inject({
        method: 'get',
        url: '/users',
        headers: { authorization: `Bearer ${token}` }
      });

      expect(statusCode).to.equal(401);
    });
  });

  describe('Routes', () => {
    describe('Register', () => {
      afterEach(() => {
        const models = server.models(true);
        const knex = server.knex();

        return Promise.all(
          Object.values(models).map(entity =>
            knex.raw(`TRUNCATE "${entity.tableName}" CASCADE`)
          )
        );
      });

      it('should fail to register user (bad email format)', async () => {
        const user = {
          email: 'email',
          password: 'password',
          pseudo: 'test'
        };

        const { statusCode } = await server.inject({
          method: 'post',
          url: `/users`,
          payload: user
        });

        expect(statusCode).to.equal(400);
      });

      it('should register user', async () => {
        const user = {
          email: 'test@test.com',
          password: 'password',
          pseudo: 'test',
          idAvatar: 10
        };

        const { statusCode } = await server.inject({
          method: 'post',
          url: `/users`,
          payload: user
        });

        expect(statusCode).to.equal(201);
      });

      it('should register 2 user with same email', async () => {
        {
          const user = {
            email: 'test@test.com',
            password: 'password',
            pseudo: 'test'
          };

          const { statusCode } = await server.inject({
            method: 'post',
            url: `/users`,
            payload: user
          });

          expect(statusCode).to.equal(201);
        }

        {
          const user2 = {
            email: 'test@test.com',
            password: 'password',
            pseudo: 'test2'
          };

          const { statusCode, result } = await server.inject({
            method: 'post',
            url: `/users`,
            payload: user2
          });

          expect(statusCode).to.equal(409);
          expect(result.message).to.equal('email already exists');
        }
      });

      it('should register 2 user with same pseudo', async () => {
        {
          const user = {
            email: 'test@test.com',
            password: 'password',
            pseudo: 'test'
          };

          const { statusCode } = await server.inject({
            method: 'post',
            url: `/users`,
            payload: user
          });

          expect(statusCode).to.equal(201);
        }

        {
          const user2 = {
            email: 'test2@test.com',
            password: 'password',
            pseudo: 'test'
          };

          const { statusCode, result } = await server.inject({
            method: 'post',
            url: `/users`,
            payload: user2
          });

          expect(statusCode).to.equal(409);
          expect(result.message).to.equal('pseudo already exists');
        }
      });
    });

    describe('Login', () => {
      afterEach(() => {
        const models = server.models(true);
        const knex = server.knex();

        return Promise.all(
          Object.values(models).map(entity =>
            knex.raw(`TRUNCATE "${entity.tableName}" CASCADE`)
          )
        );
      });

      it(`should fail to sign-in user (user doesn't exists)`, async () => {
        const user = {
          email: 'email@email.fr',
          password: 'password'
        };

        const { statusCode } = await server.inject({
          method: 'post',
          url: `/login`,
          payload: user
        });

        expect(statusCode).to.equal(404);
      });

      it('should fail to sign-in user (bad password)', async () => {
        const user = {
          email: 'test@test.com',
          password: 'password',
          pseudo: 'test'
        };

        {
          const { statusCode } = await server.inject({
            method: 'post',
            url: `/users`,
            payload: user
          });
          expect(statusCode).to.equal(201);
        }

        {
          const { statusCode } = await server.inject({
            method: 'post',
            url: `/users/login`,
            payload: { email: user.email, password: 'test' }
          });
          expect(statusCode).to.equal(404);
        }
      });

      it('should sign-in user', async () => {
        const user = {
          email: 'test@test.com',
          password: 'password',
          pseudo: 'test'
        };

        {
          const { statusCode } = await server.inject({
            method: 'post',
            url: `/users`,
            payload: user
          });
          expect(statusCode).to.equal(201);
        }

        {
          const { statusCode, result } = await server.inject({
            method: 'post',
            url: `/users/login`,
            payload: { email: user.email, password: user.password }
          });
          expect(statusCode).to.equal(200);
          expect(result.token).to.be.a.string();
          expect(result.user).to.be.an.object();
          expect(result.user.password).to.be.undefined();
        }
      });
    });

    describe('Users', () => {
      afterEach(async () => {
        const { userService } = server.services();

        try {
          await userService.deleteAll();
        } catch (err) {
          // continue
        }
      });

      it('should create a user and get all users', async () => {
        const { userService } = server.services();
        const id = await userService.signup(
          Helpers.generateUser('test@test.com', 'test')
        );

        const res = await server.inject({
          method: 'get',
          url: '/users',
          ...Helpers.authJWT(id)
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.length).to.equal(1);
      });

      it('should create a user and get the user', async () => {
        const { userService } = server.services();
        const user = await userService.signupAndFetchUser(
          Helpers.generateUser('test@test.com', 'test')
        );

        const res = await server.inject({
          method: 'get',
          url: `/users/${user.id}`,
          ...Helpers.authJWT(user.id)
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.id).to.equal(user.id);
        expect(res.result.pseudo).to.equal(user.pseudo);
        expect(res.result.idAvatar).to.equal(user.idAvatar);
        expect(res.result.email).to.equal(user.email);
        expect(res.result.password).to.not.exist();
      });

      it('should create a user and update him', async () => {
        const { userService } = server.services();
        const user = await userService.signupAndFetchUser(
          Helpers.generateUser('test@test.com', 'test')
        );

        const newInfo = {
          pseudo: 'foo',
          password: 'password'
        };

        const res = await server.inject({
          method: 'patch',
          url: `/users/${user.id}`,
          payload: newInfo,
          ...Helpers.authJWT(user.id)
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.id).to.equal(user.id);
        expect(res.result.pseudo).to.equal(newInfo.pseudo);
        expect(res.result.idAvatar).to.equal(user.idAvatar);
        expect(res.result.email).to.equal(user.email);
        expect(res.result.password).to.not.exist();
      });

      it('should create a user and update (empty payload)', async () => {
        const { userService } = server.services();
        const user = await userService.signupAndFetchUser(
          Helpers.generateUser('test@test.com', 'test')
        );

        const res = await server.inject({
          method: 'patch',
          url: `/users/${user.id}`,
          payload: {},
          ...Helpers.authJWT(user.id)
        });

        expect(res.statusCode).to.equal(200);
      });

      it('should create 2 users and fail to update one', async () => {
        const { userService } = server.services();
        const user = await userService.signupAndFetchUser(
          Helpers.generateUser('test@test.com', 'test')
        );
        const user2 = await userService.signupAndFetchUser(
          Helpers.generateUser('test2@test.com', 'test2')
        );

        const res = await server.inject({
          method: 'patch',
          url: `/users/${user.id}`,
          payload: { pseudo: 'fooBar' },
          ...Helpers.authJWT(user2.id)
        });

        expect(res.statusCode).to.equal(403);
      });

      it('should create a user and delete him', async () => {
        const { userService } = server.services();
        const user = await userService.signupAndFetchUser(
          Helpers.generateUser('test@test.com', 'test')
        );

        const res = await server.inject({
          method: 'delete',
          url: `/users/${user.id}`,
          payload: { pseudo: 'fooBar' },
          ...Helpers.authJWT(user.id)
        });

        expect(res.statusCode).to.equal(200);
      });

      it('should create 2 users and fail to delete one', async () => {
        const { userService } = server.services();
        const user = await userService.signupAndFetchUser(
          Helpers.generateUser('test@test.com', 'test')
        );
        const user2 = await userService.signupAndFetchUser(
          Helpers.generateUser('test2@test.com', 'test2')
        );

        const res = await server.inject({
          method: 'delete',
          url: `/users/${user.id}`,
          ...Helpers.authJWT(user2.id)
        });

        expect(res.statusCode).to.equal(403);
      });
    });

    describe('Rooms', () => {
      const users = [
        {
          email: 'email1@email.fr',
          password: 'password',
          pseudo: 'user1',
          idAvatar: 2
        },
        {
          email: 'email2@email.fr',
          password: 'password',
          pseudo: 'user2',
          idAvatar: 4
        },
        {
          email: 'email3@email.fr',
          password: 'password',
          pseudo: 'user3',
          idAvatar: 1
        }
      ];

      beforeEach(async () => {
        const { userService } = server.services();
        const results = [];
        for (let i = 0; i < users.length; i += 1) {
          results.push(userService.signup(users[i]));
        }
        await Promise.all(results);
      });

      afterEach(async () => {
        const { userService, roomService } = server.services();

        try {
          await userService.deleteAll();
        } catch (err) {
          // continue
        }
        try {
          await roomService.deleteAll();
        } catch (err) {
          // continue
        }
      });

      it(`should fail to create room (name already exists)`, async () => {
        const { userService } = server.services();
        const user1 = await userService.findByUsername(users[0].pseudo);
        const room = {
          name: 'roomTest',
          adminIds: [user1.id],
          userIds: []
        };

        {
          const { statusCode, result } = await server.inject({
            method: 'post',
            url: `/rooms`,
            payload: room,
            ...Helpers.authJWT()
          });
          expect(statusCode).to.equal(200);
          expect(result.name).to.equal(room.name);
        }

        {
          const { statusCode, result } = await server.inject({
            method: 'post',
            url: `/rooms`,
            payload: room,
            ...Helpers.authJWT()
          });
          expect(statusCode).to.equal(409);
          expect(result.message).to.equal('name already exists');
        }
      });

      it(`should fail to create room (some ids don't exist)`, async () => {
        const { userService } = server.services();
        const user1 = await userService.findByUsername(users[0].pseudo);
        const room = {
          name: 'roomTest',
          adminIds: [user1.id],
          userIds: [-1]
        };

        {
          const { statusCode, result } = await server.inject({
            method: 'post',
            url: `/rooms`,
            payload: room,
            ...Helpers.authJWT()
          });
          expect(statusCode).to.equal(400);
          expect(result.message).to.equal(`some ids don't exist`);
        }
      });

      it(`should create a room`, async () => {
        const { userService } = server.services();
        const user1 = await userService.findByUsername(users[0].pseudo);
        const user2 = await userService.findByUsername(users[1].pseudo);
        const room = {
          name: 'roomTest',
          adminIds: [user1.id, user2.id],
          userIds: []
        };

        let roomCreated;
        {
          const { statusCode, result } = await server.inject({
            method: 'post',
            url: `/rooms`,
            payload: room,
            ...Helpers.authJWT()
          });
          expect(statusCode).to.equal(200);
          expect(result.name).to.equal(room.name);
          roomCreated = result;
        }

        {
          const { statusCode, result } = await server.inject({
            method: 'get',
            url: `/rooms/${roomCreated.id}`,
            ...Helpers.authJWT()
          });
          expect(statusCode).to.equal(200);
          expect(result.name).to.equal(roomCreated.name);
        }
      });

      it(`should create a room and delete it`, async () => {
        const { userService, roomService } = server.services();
        const user1 = await userService.findByUsername(users[0].pseudo);
        const roomInfo = {
          name: 'roomTest',
          adminIds: [user1.id],
          userIds: []
        };
        const room = await roomService.create(roomInfo);

        const retDelete = await server.inject({
          method: 'delete',
          url: `/rooms/${room.id}`,
          ...Helpers.authJWT(user1.id)
        });
        expect(retDelete.statusCode).to.equal(200);

        const { statusCode } = await server.inject({
          method: 'get',
          url: `/rooms`,
          ...Helpers.authJWT()
        });
        expect(statusCode).to.equal(404);
      });

      it(`should create a room and modify it`, async () => {
        const { userService, roomService } = server.services();
        const user1 = await userService.findByUsername(users[0].pseudo);
        const roomInfo = {
          name: 'roomTest',
          adminIds: [user1.id],
          userIds: []
        };

        const room = await roomService.create(roomInfo);

        roomInfo.name = 'test';
        const retPatch = await server.inject({
          method: 'patch',
          url: `/rooms/${room.id}`,
          payload: roomInfo,
          ...Helpers.authJWT(user1.id)
        });
        expect(retPatch.statusCode).to.equal(200);

        const { statusCode, result } = await server.inject({
          method: 'get',
          url: `/rooms/${room.id}`,
          ...Helpers.authJWT()
        });
        expect(statusCode).to.equal(200);
        expect(result.name).to.equal(roomInfo.name);
      });

      it(`should create a room and fail to modify it (user without admin rights)`, async () => {
        const { userService, roomService } = server.services();
        const user1 = await userService.findByUsername(users[0].pseudo);
        const user2 = await userService.findByUsername(users[1].pseudo);
        const roomInfo = {
          name: 'roomTest',
          adminIds: [user1.id],
          userIds: []
        };

        const room = await roomService.create(roomInfo);

        roomInfo.adminIds = [user2.id];
        const retPatch = await server.inject({
          method: 'patch',
          url: `/rooms/${room.id}`,
          payload: roomInfo,
          ...Helpers.authJWT(user2.id)
        });
        expect(retPatch.statusCode).to.equal(403);
      });

      it(`should create rooms and get the rooms of a user`, async () => {
        const { userService, roomService } = server.services();
        const user1 = await userService.findByUsername(users[0].pseudo);
        const user2 = await userService.findByUsername(users[1].pseudo);

        await roomService.create(
          Helpers.generateRoom('roomTest', [user1.id], [])
        );
        await roomService.create(
          Helpers.generateRoom('roomTest2', [user2.id], [])
        );
        await roomService.create(
          Helpers.generateRoom('roomTest3', [user2.id], [])
        );

        const res = await server.inject({
          method: 'get',
          url: `/rooms/user`,
          ...Helpers.authJWT(user2.id)
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result.length).to.equal(2);
      });
    });
  });

  describe('Chat', () => {
    let user1;
    let user2;
    let user3;
    let room1;
    let room2;
    let connectUrl;

    before(() => {
      connectUrl = `http://${server.settings.address}:${server.settings.port}`;
    });

    beforeEach(async () => {
      const { userService, roomService } = server.services();

      const users = [
        Helpers.generateUser('test@test.com', 'user1'),
        Helpers.generateUser('test2@test.com', 'user2'),
        Helpers.generateUser('test3@test.com', 'user3')
      ];

      user1 = await userService.signupAndFetchUser(users[0]);
      user2 = await userService.signupAndFetchUser(users[1]);
      user3 = await userService.signupAndFetchUser(users[2]);

      const rooms = [
        Helpers.generateRoom('room1', [user1.id], [user2.id]),
        Helpers.generateRoom('room2', [user1.id], [user3.id])
      ];

      room1 = await roomService.create(rooms[0]);
      room2 = await roomService.create(rooms[1]);
    });

    afterEach(async () => {
      const { userService, roomService } = server.services();

      try {
        await userService.deleteAll();
      } catch (err) {
        // continue
      }
      try {
        await roomService.deleteAll();
      } catch (err) {
        // continue
      }
    });

    it('should fail to connect user', { timeout: 3000 }, async () => {
      try {
        Io.connect(connectUrl, {
          transports: ['websocket'],
          query: {
            room: room1.id
          }
        });
        Code.fail();
      } catch (err) {
        expect(err).to.be.an.error();
      }
    });

    it('should fail to connect user', { timeout: 3000 }, async () => {
      const { userService } = server.services();

      try {
        Io.connect(connectUrl, {
          transports: ['websocket'],
          query: {
            token: await userService.createToken(user2.id),
            room: room2.id
          }
        });
        Code.fail();
      } catch (err) {
        expect(err).to.be.an.error();
      }
    });

    it('should connect user to chat', { timeout: 3000 }, async () => {
      const team = new Teamwork();
      const { userService } = server.services();

      const socket = Io.connect(connectUrl, {
        transports: ['websocket'],
        query: {
          token: await userService.createToken(user1.id),
          room: room1.id
        }
      });

      socket.on('users', () => {
        team.attend();
      });

      await team.work;
      socket.close();
    });

    it('should connect users + send a message', { timeout: 3000 }, async () => {
      const team = new Teamwork();
      const { userService } = server.services();

      const socket = Io.connect(connectUrl, {
        transports: ['websocket'],
        query: {
          token: await userService.createToken(user1.id),
          room: room1.id
        }
      });

      const socket2 = Io.connect(connectUrl, {
        transports: ['websocket'],
        query: {
          token: await userService.createToken(user2.id),
          room: room1.id
        }
      });

      socket.on('users', () => {
        socket.emit('message', 'test');
      });

      socket2.on('message', message => {
        expect(message.message).to.equal('test');
        expect(message.id).to.equal(user1.id);
        team.attend();
      });

      await team.work;
      socket.close();
      socket2.close();
    });

    it(
      'should connect users + fail to send empty message',
      { timeout: 3000 },
      async () => {
        const { userService } = server.services();

        const socket = Io.connect(connectUrl, {
          transports: ['websocket'],
          query: {
            token: await userService.createToken(user1.id),
            room: room1.id
          }
        });

        const socket2 = Io.connect(connectUrl, {
          transports: ['websocket'],
          query: {
            token: await userService.createToken(user2.id),
            room: room1.id
          }
        });

        socket.on('users', () => {
          socket.emit('message', '');
        });

        socket2.on('message', () => {
          Code.fail();
        });

        await Hoek.wait(300);

        socket.close();
        socket2.close();
      }
    );

    it('should connect user to chat', { timeout: 3000 }, async () => {
      const team = new Teamwork();
      const { userService } = server.services();

      const socket = Io.connect(connectUrl, {
        transports: ['websocket'],
        query: {
          token: await userService.createToken(user1.id),
          room: room1.id
        }
      });

      socket.on('disconnect', () => {
        team.attend();
      });

      const socket2 = Io.connect(connectUrl, {
        transports: ['websocket'],
        query: {
          token: await userService.createToken(user1.id),
          room: room2.id
        }
      });

      await team.work;
      socket2.close();
    });

    it(
      'should disconnect all users if room is deleted',
      { timeout: 3000 },
      async () => {
        const team = new Teamwork();
        const { userService } = server.services();

        const socket = Io.connect(connectUrl, {
          transports: ['websocket'],
          query: {
            token: await userService.createToken(user1.id),
            room: room1.id
          }
        });

        socket.on('disconnect', () => {
          team.attend();
        });

        const socket2 = Io.connect(connectUrl, {
          transports: ['websocket'],
          query: {
            token: await userService.createToken(user2.id),
            room: room1.id
          }
        });

        socket2.on('disconnect', () => {
          team.attend();
        });

        const socket3 = Io.connect(connectUrl, {
          transports: ['websocket'],
          query: {
            token: await userService.createToken(user3.id),
            room: room2.id
          }
        });

        await Hoek.wait(400);

        await server.inject({
          method: 'delete',
          url: `/rooms/${room1.id}`,
          ...Helpers.authJWT(user1.id)
        });

        await Hoek.wait(200);

        await team.work;
        socket3.close();
      }
    );

    it('should cover all uncover internals', () => {
      const map1 = new Map();
      map1.set(-1, { room: { id: -1 } });

      const map2 = new Map();
      map2.set(-1, {});

      const map3 = new Map();
      map3.set(-1, { room: { id: -1 }, socket: null });

      server.chat.internals.disconnectUsers(1, []);
      server.chat.internals.disconnectUsers(1, map1);
      server.chat.internals.disconnectUsers(1, map2);
      server.chat.internals.disconnectUsers(-1, map3);
      server.chat.internals.sendJoin(1, 1, null);
      server.chat.internals.sendJoin(1, 1, {});
      server.chat.internals.sendMessage(1, 1, null, '');
      server.chat.internals.sendMessage(1, 1, {}, '');
      server.chat.internals.sendLeave(1, 1, null);
      server.chat.internals.sendLeave(1, 1, {});
      server.chat.internals.sendUsers(1, 1, null, []);
      server.chat.internals.sendUsers(1, 1, null, map1);
      server.chat.internals.sendUsers(1, 1, null, map2);
      server.chat.internals.log({}, {}, {}, {}, -1);
    });

    describe('History', () => {
      it('should fail to create empty message', async () => {
        const { historyService } = server.services();

        const history = {
          roomId: room1.id,
          userId: user1.id,
          message: ''
        };

        try {
          await historyService.create(history);
        } catch (err) {
          expect(err).to.be.an.error();
        }
      });

      it('should not fail to create long message', async () => {
        const { historyService } = server.services();

        const history = {
          roomId: room1.id,
          userId: user1.id,
          message:
            'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'
        };

        try {
          await historyService.create(history);
        } catch (err) {
          Code.fail();
        }
      });
    });
  });
});
