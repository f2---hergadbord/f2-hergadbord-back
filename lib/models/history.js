const Joi = require('@hapi/joi');
const { Model } = require('./helpers');
const HistorySchema = require('../schemas/history');

module.exports = class History extends Model {
  static get tableName() {
    return 'history';
  }

  static get joiSchema() {
    return Joi.object(HistorySchema.schema);
  }
};
