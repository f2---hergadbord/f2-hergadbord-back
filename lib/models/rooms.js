const Joi = require('@hapi/joi');
const { Model } = require('./helpers');
const RoomSchema = require('../schemas/rooms');
const HistoryModel = require('./history');

module.exports = class Rooms extends Model {
  static get tableName() {
    return 'rooms';
  }

  static get joiSchema() {
    return Joi.object(RoomSchema.schema);
  }

  static get relationMappings() {
    return {
      message: {
        relation: Model.HasManyRelation,
        modelClass: HistoryModel,
        join: {
          from: 'rooms.id',
          to: 'history.roomId'
        }
      }
    };
  }
};
