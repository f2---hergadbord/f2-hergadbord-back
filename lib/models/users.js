const Joi = require('@hapi/joi');
const { Model } = require('./helpers');
const UserSchema = require('../schemas/users');

module.exports = class Users extends Model {
  static get tableName() {
    return 'users';
  }

  static get joiSchema() {
    return Joi.object(UserSchema.schema);
  }
};
