const Joi = require('@hapi/joi');
const Schema = require('./helpers');

module.exports = class Users extends Schema {
  static get schema() {
    return {
      id: Joi.number().integer(),
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string()
        .min(1)
        .required(),
      pseudo: Joi.string()
        .min(1)
        .required(),
      idAvatar: Joi.number()
        .integer()
        .min(0)
    };
  }
};
