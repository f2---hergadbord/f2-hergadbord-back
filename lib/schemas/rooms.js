const Joi = require('@hapi/joi');
const Schema = require('./helpers');

module.exports = class Rooms extends Schema {
  static get schema() {
    return {
      id: Joi.number().integer(),
      adminIds: Joi.array()
        .items(
          Joi.number()
            .integer()
            .disallow(Joi.ref('userIds'))
        )
        .min(1)
        .unique()
        .required(),
      userIds: Joi.array()
        .items(
          Joi.number()
            .integer()
            .disallow(Joi.ref('adminIds'))
        )
        .unique()
        .required(),
      name: Joi.string()
        .min(1)
        .required()
    };
  }
};
