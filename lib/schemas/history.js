const Joi = require('@hapi/joi');
const Schema = require('./helpers');

module.exports = class History extends Schema {
  static get schema() {
    return {
      id: Joi.number().integer(),
      roomId: Joi.number().integer(),
      userId: Joi.number().integer(),
      message: Joi.string()
    };
  }
};
