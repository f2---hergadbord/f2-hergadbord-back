// TODO : test this functions independently and enable coverage
/* $lab:coverage:off$ */
const Joi = require('@hapi/joi');

function omit(obj, keys) {
  const omitted = { ...obj };

  let k = keys;
  if (!Array.isArray(keys)) {
    k = [keys];
  }

  Object.keys(obj).forEach(key => {
    if (k.indexOf(key) !== -1) {
      delete omitted[key];
    }
  });

  return omitted;
}

function pick(obj, keys) {
  const picked = {};

  let k = keys;
  if (!Array.isArray(keys)) {
    k = [keys];
  }

  k.forEach(key => {
    if (key in obj) {
      picked[key] = obj[key];
    }
  });

  return picked;
}

module.exports = class {
  static get schema() {
    return {};
  }

  static omit(...keys) {
    return Joi.object(
      omit(this.schema, [
        ...keys,
        ...Object.keys(this.schema).filter(
          key =>
            this.schema[key].describe().meta &&
            this.schema[key].describe().meta.some(elem => elem.omit)
        )
      ])
    );
  }

  static pick(...keys) {
    return Joi.object(pick(this.schema, keys));
  }
};
/* $lab:coverage:on$ */
