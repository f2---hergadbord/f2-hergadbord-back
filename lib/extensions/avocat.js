const Toys = require('toys');
const Avocat = require('avocat');

module.exports = Toys.onPreResponse(({ response }, h) => {
  Avocat.rethrow(response);

  return h.continue;
});
