const SocketIO = require('socket.io');
const JWT = require('jsonwebtoken');

const internals = {};

class LogType {
  static get Join() {
    return 0;
  }

  static get Message() {
    return 1;
  }

  static get Leave() {
    return 2;
  }

  static get Disconnect() {
    return 3;
  }
}

module.exports = [
  {
    type: 'onPostStart',
    method: async server => {
      const { roomService, userService } = server.services();
      const userDatas = new Map();
      const io = SocketIO.listen(server.listener);

      server.decorate('server', 'chat', {
        io,
        users: userDatas,
        disconnect: internals.disconnectUsers,
        internals
      });

      // Check auth, user and room before connection
      io.use(async (socket, next) => {
        const { token, room } = socket.handshake.query;

        if (!token) {
          return next(new Error('Authentication error'));
        }

        try {
          JWT.verify(token, server.realm.pluginOptions.jwtKey);
          const decoded = JWT.decode(token, { algorithms: ['HS256'] });
          await roomService.checkConnectionRights(room, decoded.id);
        } catch (err) {
          return next(new Error('Authentication error'));
        }
        return next();
      });

      // Manage rooms / users
      io.sockets.on('connection', async socket => {
        const room = await roomService.findById(socket.handshake.query.room);
        const decoded = JWT.decode(socket.handshake.query.token, {
          algorithms: ['HS256']
        });
        const user = await userService.findById(decoded.id);

        // Check if user already in room
        const sockData = userDatas.get(user.id);
        if (sockData) {
          const { socket: previousSocket, room: previousRoom } = sockData;
          internals.sendLeave(user, previousRoom, previousRoom);
          internals.log(user, previousRoom, null, server, LogType.Disconnect);
          previousSocket.disconnect();
        }

        userDatas.set(user.id, { socket, room });

        // Join room
        socket.join(room.name);
        internals.sendJoin(user, room, socket);
        internals.log(user, room, null, server, LogType.Join);

        internals.sendUsers(user, room, socket, userDatas);

        socket.on('message', async message => {
          const { historyService } = server.services();

          try {
            await historyService.create({
              userId: user.id,
              roomId: room.id,
              message
            });
            internals.sendMessage(user, room, socket, message);
            internals.log(user, room, message, server, LogType.Message);
          } catch (err) {
            server.log(['message', 'err'], err);
          }
        });

        socket.on('disconnect', () => {
          internals.sendLeave(user, room, socket);
          internals.log(user, room, null, server, LogType.Leave);
          userDatas.delete(user.id);
        });
      });
    }
  }
];

// Internals methods
internals.disconnectUsers = (roomId, users) => {
  users.forEach(value => {
    if (value.room && value.room.id === roomId && value.socket) {
      value.socket.disconnect();
    }
  });
};

internals.sendJoin = (user, room, socket) => {
  if (socket && socket.broadcast) {
    socket.broadcast
      .to(room.name)
      .emit('join', { id: user.id, pseudo: user.pseudo });
  }
};

internals.sendMessage = (user, room, socket, message) => {
  if (socket && socket.broadcast) {
    socket.broadcast
      .to(room.name)
      .emit('message', { id: user.id, pseudo: user.pseudo, message });
  }
};

internals.sendLeave = (user, room, socket) => {
  if (socket && socket.broadcast) {
    socket.broadcast
      .to(room.name)
      .emit('leave', { id: user.id, pseudo: user.pseudo });
  }
};

internals.sendUsers = (user, room, socket, users) => {
  const connected = [];
  users.forEach((value, key) => {
    if (value.room && value.room.id === room.id && key !== user.id) {
      connected.push(key);
    }
  });
  if (socket) {
    socket.emit('users', { users: connected });
  }
};

internals.log = (user, room, data, server, logType) => {
  switch (logType) {
    case LogType.Join:
      server.log(
        ['connection'],
        `user ${user.id} has joined the room ${room.id}`
      );
      break;
    case LogType.Message:
      server.log(['message'], `user ${user.id} has sent a message : ${data}`);
      break;
    case LogType.Leave:
      server.log(
        ['disconnection'],
        `user ${user.id} has left the room ${room.id}`
      );
      break;
    case LogType.Disconnect:
      server.log(
        ['server', 'disconnection'],
        `The server has disconnected ${user.id} from ${room.id}`
      );
      break;
    default:
      break;
  }
};
