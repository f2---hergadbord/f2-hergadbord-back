module.exports = {
  async up(knex) {
    await knex.schema.alterTable('rooms', table => {
      table.unique(['id', 'name']);
    });
  },

  async down(knex) {
    await knex.schema.alterTable('rooms', table => {
      table.dropUnique('id_name');
    });
  }
};
