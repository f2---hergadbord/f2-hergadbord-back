module.exports = {
  async up(knex) {
    await knex.schema.createTable('history', table => {
      table.increments('id').primary();
      table.integer('roomId');
      table.integer('userId');
      table.text('message');

      table
        .foreign('roomId')
        .references('id')
        .inTable('rooms')
        .onDelete('cascade');

      table
        .foreign('userId')
        .references('id')
        .inTable('users')
        .onDelete('cascade');
    });
  },

  async down(knex) {
    await knex.schema.dropTableIfExists('history');
  }
};
