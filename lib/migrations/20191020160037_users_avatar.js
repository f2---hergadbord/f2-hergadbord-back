module.exports = {
  async up(knex) {
    await knex.schema.alterTable('users', table => {
      table.integer('idAvatar').defaultTo(1);
    });
  },

  async down(knex) {
    await knex.schema.alterTable('users', table => {
      table.dropColumn('idAvatar');
    });
  }
};
