module.exports = {
  async up(knex) {
    await knex.schema.createTable('users', table => {
      table.increments('id').primary();
      table.string('email');
      table.string('password');
      table.string('pseudo');
    });
  },

  async down(knex) {
    await knex.schema.dropTableIfExists('users');
  }
};
