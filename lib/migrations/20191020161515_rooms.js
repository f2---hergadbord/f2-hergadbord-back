module.exports = {
  async up(knex) {
    await knex.schema.createTable('rooms', table => {
      table.increments('id').primary();
      table.jsonb('adminIds');
      table.jsonb('userIds');
      table.string('name');
    });
  },

  async down(knex) {
    await knex.schema.dropTableIfExists('rooms');
  }
};
