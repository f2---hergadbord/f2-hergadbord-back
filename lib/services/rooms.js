const Boom = require('@hapi/boom');
const Schmervice = require('schmervice');

module.exports = class RoomService extends Schmervice.Service {
  findAll(trx) {
    const { Rooms } = this.server.models();

    return Rooms.query(trx)
      .throwIfNotFound()
      .eager('message(selectUserIdAndMessage)', {
        selectUserIdAndMessage: builder => {
          builder.select('id', 'userId', 'message');
        }
      });
  }

  findById(id, trx) {
    return this.findAll(trx).findById(id);
  }

  findByName(name, trx) {
    return this.findAll(trx)
      .where({ name })
      .first();
  }

  findUserRooms(userId, trx) {
    return this.findAll(trx)
      .whereJsonSupersetOf('adminIds', [userId])
      .orWhereJsonSupersetOf('userIds', [userId]);
  }

  deleteAll(trx) {
    return this.findAll(trx).delete();
  }

  deleteOne(id, trx) {
    this.disconnectUsers(id);
    return this.findById(id, trx).delete();
  }

  async create(roomInfo, trx) {
    await this.checkRoomAlreadyExists(roomInfo, trx);
    await this.checkUsersAndAdmins(roomInfo, trx);

    const { Rooms } = this.server.models();
    return Rooms.query(trx).insert(roomInfo);
  }

  async update(id, roomInfo, trx) {
    const { Rooms } = this.server.models();

    await Rooms.query(trx)
      .throwIfNotFound()
      .where({ id })
      .patch(roomInfo);
  }

  async checkRoomAlreadyExists(roomInfo, trx) {
    let isRoom = false;

    try {
      isRoom = await this.findByName(roomInfo.name, trx);
    } catch (err) {
      // continue regardless of error
    }
    if (isRoom) {
      throw Boom.conflict('name already exists');
    }
  }

  async checkUsersAndAdmins({ adminIds, userIds }, trx) {
    const { userService } = this.server.services();
    const results = [];
    for (let i = 0; i < adminIds.length; i += 1) {
      results.push(userService.findById(adminIds[i], trx));
    }
    for (let i = 0; i < userIds.length; i += 1) {
      results.push(userService.findById(userIds[i], trx));
    }
    try {
      await Promise.all(results);
    } catch (err) {
      throw Boom.badRequest(`some ids don't exist`);
    }
  }

  async checkAdminRights(roomId, userId) {
    const room = await this.findById(roomId);
    if (!room.adminIds.includes(userId))
      throw Boom.forbidden("You don't have the rights to modify a room");
  }

  async checkConnectionRights(roomId, userId) {
    const room = await this.findById(roomId);
    if (!room.adminIds.includes(userId) && !room.userIds.includes(userId))
      throw Boom.forbidden("You can't access this room");
  }

  disconnectUsers(roomId) {
    this.server.chat.disconnect(roomId, this.server.chat.users);
  }
};
