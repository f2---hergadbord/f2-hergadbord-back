const Bcrypt = require('bcrypt');
const Boom = require('@hapi/boom');
const Schmervice = require('schmervice');
const JWT = require('jsonwebtoken');

module.exports = class UserService extends Schmervice.Service {
  findById(id, trx) {
    return this.findAll(trx).findById(id);
  }

  findByUsername(pseudo, trx) {
    return this.findAll(trx)
      .first()
      .where({ pseudo });
  }

  findByEmail(email, trx) {
    return this.findAll(trx)
      .first()
      .where({ email });
  }

  findAll(trx) {
    const { Users } = this.server.models();

    return Users.query(trx).throwIfNotFound();
  }

  deleteAll(trx) {
    return this.findAll(trx).delete();
  }

  deleteOne(id, trx) {
    return this.deleteAll(trx).where({ id });
  }

  async signup({ password, ...userInfo }, trx) {
    const { Users } = this.server.models();
    const { id } = await Users.query(trx).insert({ ...userInfo, password });

    await this.changePassword(id, password, trx);
    return id;
  }

  async signupAndFetchUser(userInfo, trx) {
    await this.checkUserAlreadyExists(userInfo, trx);
    const id = await this.signup(userInfo, trx);

    return this.findById(id, trx);
  }

  async update(id, { password, ...userInfo }, trx) {
    const { Users } = this.server.models();

    if (Object.keys(userInfo).length > 0) {
      await Users.query(trx)
        .throwIfNotFound()
        .where({ id })
        .patch(userInfo);
    }

    if (password) {
      await this.changePassword(id, password, trx);
    }
  }

  async login({ email, password }, trx) {
    const { Users } = this.server.models();

    const user = await this.findByEmail(email, trx);

    if (!Bcrypt.compareSync(password, user.password)) {
      throw Users.createNotFoundError();
    }

    return user;
  }

  async createToken(id) {
    return JWT.sign({ id }, this.options.jwtKey, {
      algorithm: 'HS256',
      expiresIn: '7d'
    });
  }

  async changePassword(id, password, trx) {
    const { Users } = this.server.models();

    const hashPwd = Bcrypt.hashSync(password, 10);

    await Users.query(trx)
      .throwIfNotFound()
      .where({ id })
      .patch({
        password: hashPwd
      });

    return id;
  }

  async checkUserAlreadyExists(userInfo, trx) {
    let isEmail = false;
    let isPseudo = false;

    try {
      isEmail = await this.findByEmail(userInfo.email, trx);
    } catch (err) {
      // continue regardless of error
    }
    if (isEmail) {
      throw Boom.conflict('email already exists');
    }
    try {
      isPseudo = await this.findByUsername(userInfo.pseudo, trx);
    } catch (err) {
      // continue regardless of error
    }
    if (isPseudo) {
      throw Boom.conflict('pseudo already exists');
    }
  }
};
