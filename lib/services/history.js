const Schmervice = require('schmervice');

module.exports = class HistoryService extends Schmervice.Service {
  create(history, trx) {
    const { History } = this.server.models();

    return History.query(trx).insert(history);
  }
};
