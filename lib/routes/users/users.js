const Boom = require('@hapi/boom');
const Helpers = require('../helpers');
const UserSchema = require('../../schemas/users');

module.exports = Helpers.withDefaults(['api', 'users'])([
  {
    method: 'get',
    path: '/users',
    handler: async request => {
      const { userService } = request.services();

      return userService.findAll().select('id', 'pseudo', 'idAvatar', 'email');
    }
  },
  {
    method: 'get',
    path: '/users/{id}',
    options: {
      validate: {
        params: UserSchema.pick('id')
      }
    },
    handler: async request => {
      const { userService } = request.services();

      return userService
        .findById(request.params.id)
        .select('id', 'pseudo', 'idAvatar', 'email');
    }
  },
  {
    method: 'patch',
    path: '/users/{id}',
    options: {
      validate: {
        params: UserSchema.pick('id'),
        payload: UserSchema.omit('id').optionalKeys(
          'email',
          'password',
          'pseudo',
          'idAvatar'
        )
      }
    },
    handler: async (request, h) => {
      const { userService } = request.services();

      if (request.params.id !== request.auth.credentials.id)
        throw Boom.forbidden("Can't modify other user");
      await h.ctx(ctx =>
        userService.update(request.params.id, request.payload, ctx.trx)
      );
      const user = await userService
        .findById(request.params.id)
        .select('id', 'pseudo', 'idAvatar', 'email');
      return user;
    }
  },
  {
    method: 'delete',
    path: '/users/{id}',
    options: {
      validate: {
        params: UserSchema.pick('id')
      }
    },
    handler: async (request, h) => {
      const { userService } = request.services();

      if (request.params.id !== request.auth.credentials.id)
        throw Boom.forbidden("Can't modify other user");
      return h.ctx(ctx => userService.deleteOne(request.params.id, ctx.trx));
    }
  }
]);
