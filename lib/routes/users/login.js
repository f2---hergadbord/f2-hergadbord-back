const Helpers = require('../helpers');
const UserSchema = require('../../schemas/users');

module.exports = Helpers.withDefaults(['api', 'login'])({
  method: 'post',
  path: '/users/login',
  options: {
    validate: {
      payload: UserSchema.pick('email', 'password')
    },
    auth: false
  },
  handler: async (request, h) => {
    const { email, password } = request.payload;
    const { userService } = request.services();

    const user = await h.ctx(ctx =>
      userService.login({ email, password }, ctx.trx)
    );
    const token = await userService.createToken(user.id);

    delete user.password;

    return {
      user,
      token
    };
  }
});
