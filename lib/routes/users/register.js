const Helpers = require('../helpers');
const UserSchema = require('../../schemas/users');

module.exports = Helpers.withDefaults(['api', 'register'])({
  method: 'post',
  path: '/users',
  options: {
    validate: {
      payload: UserSchema.omit('id')
    },
    auth: false
  },
  handler: async (request, h) => {
    const { userService } = request.services();

    const user = await h.ctx(ctx =>
      userService.signupAndFetchUser(request.payload, ctx.trx)
    );
    const token = await userService.createToken(user.id);

    delete user.password;

    return h.response({ user, token }).code(201);
  }
});
