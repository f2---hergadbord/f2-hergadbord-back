const Toys = require('toys');

exports.withDefaults = tags => {
  return Toys.withRouteDefaults({
    options: {
      tags,
      cors: true,
      auth: 'jwt'
    }
  });
};
exports.currentUserId = Toys.reacher('auth.credentials.id', { default: null });
