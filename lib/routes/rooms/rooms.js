const Helpers = require('../helpers');
const RoomSchema = require('../../schemas/rooms');

module.exports = Helpers.withDefaults(['api', 'rooms'])([
  {
    method: 'get',
    path: '/rooms',
    handler: async request => {
      const { roomService } = request.services();

      return roomService.findAll();
    }
  },
  {
    method: 'get',
    path: '/rooms/{id}',
    options: {
      validate: {
        params: RoomSchema.pick('id')
      }
    },
    handler: async request => {
      const { roomService } = request.services();

      return roomService.findById(request.params.id);
    }
  },
  {
    method: 'get',
    path: '/rooms/user',
    handler: async request => {
      const { roomService } = request.services();

      return roomService.findUserRooms(request.auth.credentials.id);
    }
  },
  {
    method: 'post',
    path: '/rooms',
    options: {
      validate: {
        payload: RoomSchema.omit('id')
      }
    },
    handler: async (request, h) => {
      const { roomService } = request.services();

      return h.ctx(ctx => roomService.create(request.payload, ctx.trx));
    }
  },
  {
    method: 'patch',
    path: '/rooms/{id}',
    options: {
      validate: {
        params: RoomSchema.pick('id'),
        payload: RoomSchema.omit('id').optionalKeys(
          'adminIds',
          'userIds',
          'name'
        )
      }
    },
    handler: async (request, h) => {
      const { roomService } = request.services();
      await roomService.checkAdminRights(
        request.params.id,
        request.auth.credentials.id
      );
      await h.ctx(ctx =>
        roomService.update(request.params.id, request.payload, ctx.trx)
      );
      return roomService.findById(request.params.id);
    }
  },
  {
    method: 'delete',
    path: '/rooms/{id}',
    options: {
      validate: {
        params: RoomSchema.pick('id')
      }
    },
    handler: async (request, h) => {
      const { roomService } = request.services();

      await roomService.checkAdminRights(
        request.params.id,
        request.auth.credentials.id
      );
      return h.ctx(ctx => roomService.deleteOne(request.params.id, ctx.trx));
    }
  }
]);
