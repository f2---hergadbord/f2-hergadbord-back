const Objection = require('objection');

module.exports = {
  method(cb) {
    return Objection.transaction(this.knex(), trx =>
      cb({ trx, request: this.request })
    );
  }
};
